@extends('layouts.cashier')
@section('content')

<style>
	@page {
		size: A4;
		margin: 0;
	}

	@media print {
		#printButton {
			visibility: hidden;
		}

		#kt_header_mobile {
			visibility: hidden;

		}
	}
</style>
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container-fluid">
		<div class="row">

			<div class="px-2" style="width:500px;">
				<!-- begin::Card-->
				<div class="card card-custom overflow-hidden p-0">
					<div class="card-body p-0">
						<!-- begin: Invoice-->
						<!-- begin: Invoice header-->
						<div class="row justify-content-center px-4 pt-md-27 px-md-0">
							<div class="col-md-9">
								<div class="d-flex justify-content-between flex-column flex-md-row">
									<h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>
									<div class="d-flex flex-column align-items-md-end px-0">
										<!--begin::Logo-->
										<a href="#" class="mb-5">
											<img src="{{ asset('storage/'. session('settings_restaurant_logo'))}}" style="height:50px;width:50px" alt="">
										</a>

									</div>
								</div>

							</div>
						</div>
						<!-- end: Invoice header-->
						<!-- begin: Invoice body-->
						<div class="row justify-content-center px-8  px-md-0">
							<div class="col-md-9">
								<div class="table-responsive">
									<h3>TOKEN NO: {{ $order->token_no }}</h3>

									<table class="table" style="font-size:14px;">
										<thead style="font-size:14px;">
											<tr>
												<th class="pl-0 font-weight-bold text-muted text-uppercase"> Name</th>
												<th class="text-right font-weight-bold text-muted text-uppercase">Price</th>
												<th class="text-right font-weight-bold text-muted text-uppercase">Quantity</th>
												<th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Scheme</th>
												<th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Total</th>

											</tr>
										</thead>
										<tbody>
											@foreach($order->foods as $key => $food)
											<tr>
												<td class="pl-0 pt-7">{{ $food->name }}</td>
												<td class="text-right pt-7">{{ $food->price }}</td>
												<td class="text-right pt-7">{{ $food->pivot->quantity }}</td>
												<td class="text-right pt-7">{{ $food->pivot->scheme }}</td>

												<td class="text-danger pr-0 pt-7 text-right">{{ $food->price*$food->pivot->quantity }} </td>
											</tr>
											@endforeach

											@foreach($cartConditions as $key => $condition)

											<tr>
												<td class=" pt-2" colspan="5">

													{{$condition->getName()}}

												</td>
											</tr>
											@endforeach

											<tr>
												<td class="pl-0 pt-7" colspan="5">
													<b> Grand Total:</b> Rs {{ $order->grand_total }}</td>
											</tr>
										</tbody>

									</table>
								</div>
							</div>
						</div>
						<!-- end: Invoice body-->


						<!-- begin: Invoice action-->
						<div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
							<div class="col-md-9">
								<div class="d-flex justify-content-between">
									<button type="button" class="btn btn-primary font-weight-bold" id="printButton" onclick="window.print();">Print Invoice</button>
								</div>
							</div>
						</div>
						<!-- end: Invoice action-->
						<!-- end: Invoice-->
					</div>
				</div>
				<!-- end::Card-->
			</div>
			<div class="px-2" style="width:500px;">
				<!-- begin::Card-->
				<div class="card card-custom overflow-hidden p-0">
					<div class="card-body p-0">
						<!-- begin: Invoice-->
						<!-- begin: Invoice header-->
						<div class="row justify-content-center px-4 pt-md-27 px-md-0">
							<div class="col-md-9">
								<div class="d-flex justify-content-between flex-column flex-md-row">
									<h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>
									<div class="d-flex flex-column align-items-md-end px-0">
										<!--begin::Logo-->
										<a href="#" class="mb-5">

											<img src="{{ asset('storage/'. session('settings_restaurant_logo'))}}" style="height:50px;width:50px" alt="">
										</a>

									</div>
								</div>

							</div>
						</div>
						<!-- end: Invoice header-->
						<!-- begin: Invoice body-->
						<div class="row justify-content-center px-8  px-md-0">
							<div class="col-md-9">
								<div class="table-responsive">
									<h3>TOKEN NO: {{ $order->token_no }}</h3>

									<table class="table" style="font-size:14px;">
										<thead style="font-size:14px;">
											<tr>
												<th class="pl-0 font-weight-bold text-muted text-uppercase"> Name</th>
												<th class="text-right font-weight-bold text-muted text-uppercase">Price</th>
												<th class="text-right font-weight-bold text-muted text-uppercase">Quantity</th>
												<th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Scheme</th>
												<th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Total</th>

											</tr>
										</thead>
										<tbody>
											@foreach($order->foods as $key => $food)
											<tr>
												<td class="pl-0 pt-7">{{ $food->name }}</td>
												<td class="text-right pt-7">{{ $food->price }}</td>
												<td class="text-right pt-7">{{ $food->pivot->quantity }}</td>
												<td class="text-right pt-7">{{ $food->pivot->scheme }}</td>

												<td class="text-danger pr-0 pt-7 text-right">{{ $food->price*$food->pivot->quantity }} </td>
											</tr>
											@endforeach

											@foreach($cartConditions as $key => $condition)

											<tr>
												<td class=" pt-2" colspan="5">

													{{$condition->getName()}}

												</td>
											</tr>
											@endforeach

											<tr>
												<td class="pl-0 pt-7" colspan="5">
													<b> Grand Total:</b> Rs {{ $order->grand_total }}</td>
											</tr>
										</tbody>

									</table>
								</div>
							</div>
						</div>
						<!-- end: Invoice body-->


						<!-- begin: Invoice action-->
						<div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
							<div class="col-md-9">
								<div class="d-flex justify-content-between">
									<form action="{{route('print')}}" method="POST">
									@csrf
									<button type="submit" class="btn btn-primary font-weight-bold" >Print Invoice</button>
									</form>
									
								</div>
							</div>
						</div>
						<!-- end: Invoice action-->
						<!-- end: Invoice-->
					</div>
				</div>
				<!-- end::Card-->
			</div>

		</div>
	</div>
	<!--end::Container-->
</div>



@endsection