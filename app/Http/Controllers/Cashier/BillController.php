<?php

namespace App\Http\Controllers\Cashier;

use App\Food;
use App\FoodOrder;
use App\Http\Controllers\Controller;
use App\Order;
use App\Scheme;
use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use charlieuki\ReceiptPrinter\ReceiptPrinter as ReceiptPrinter;

class BillController extends Controller
{
    // public function index(){
    //     return view('pages.billing.index')->with('foods',Food::all());
    // }

    public function index()
    {
        return view('pages.billing.index')->with('foods', Food::all());
    }

    public function checkout(Request $request)
    {
        // dd(\Cart::session(auth()->id())->getContent());
        // dd($cartConditions);
        $items = \Cart::session(auth()->id())->getContent();
        if ($items->count() === 0) {
            return redirect()->back();
        }
        foreach ($items as $item) {
            
                if ($request->has('scheme')) {
                    $status = true;
                } else {
                    $status = false;
                }
                $scheme = Scheme::where('food_id', $item->id)->whereDate('start_date', '<=', Carbon::today())->whereDate('end_date', '>=', Carbon::today())->first();
                if ($scheme && $status) {
                    $add_quantity = intval($item->quantity / $scheme->quantity);
                    // dd($add_quantity);
                } else {
                    $add_quantity = 0;
                }
                
            
            $scheme_quantity[] = $add_quantity;
        }
        // $i = 0;

        // foreach ($items as $item) {
        //     $stock = Stock::where('food_id', $item->id)->whereDate('from_date', Carbon::today())->first();
        //     $stock->remaining_quantity = $stock->remaining_quantity - ($item->quantity + $scheme_quantity[$i]);
        //     $stock->save();
        //     $i++;
        // }



        $lastOrder = Order::orderBy('created_at', 'desc')->whereDate('created_at', Carbon::today())->first();
        if ($lastOrder) {
            $token_no = $lastOrder->token_no + 1;
        } else {
            $token_no = 1;
        }
        $order = Order::create([
            'user_id' => auth()->id(),
            'grand_total' => \Cart::session(auth()->id())->getSubTotal(),
            'sub_total' => \Cart::session(auth()->id())->getTotal(),
            'token_no' => $token_no
        ]);
        $i = 0;

        foreach ($items as $item) {
            FoodOrder::create([
                'food_id' => $item->id,
                'order_id' => $order->id,
                'price' => $item->price,
                'quantity' => $item->quantity,
                'scheme' => $scheme_quantity[$i]
            ]);
            $i++;
        }
        // dd($scheme_quantity);


        $cartConditions = \Cart::session(auth()->id())->getConditions();



        // \Cart::session(auth()->id())->clear();
        // \Cart::session(auth()->id())->clearCartConditions();
        $scheme_quantity[] = [];
        $foods = $order->foods;



        return view('pages.billing.bill', compact('order', 'items', 'cartConditions', 'foods'));
    }

    public function search(Request $request){
        $searchTerm = '%'.$request->search.'%';

        $foods=Food::where('name','like',$searchTerm)->pluck('id');
        $stocks=[];
        foreach($foods as $food_id){
            $stock=Stock::where('food_id',$food_id)->whereDate('from_date',Carbon::today())->where('remaining_quantity','>',0)->get();
            
      
            if($stock->count()==0){
            
         
            }else{
                array_push($stocks,$stock);

            }
            return view('pages.billing.index')->with('foods');

        }
  
        return $stocks;
    
}
public function print()
{
  // Set params
$mid = '123123456';
$store_name = 'YOURMART';
$store_address = 'Mart Address';
$store_phone = '1234567890';
$store_email = 'yourmart@email.com';
$store_website = 'yourmart.com';
$tax_percentage = 10;
$transaction_id = 'TX123ABC456';

// Set items
$items = [
    [
        'name' => 'French Fries (tera)',
        'qty' => 2,
        'price' => 65000,
    ],
    [
        'name' => 'Roasted Milk Tea (large)',
        'qty' => 1,
        'price' => 24000,
    ],
    [
        'name' => 'Honey Lime (large)',
        'qty' => 3,
        'price' => 10000,
    ],
    [
        'name' => 'Jasmine Tea (grande)',
        'qty' => 3,
        'price' => 8000,
    ],
];

// Init printer
$printer = new ReceiptPrinter;
$printer->init(
    config('receiptprinter.connector_type'),
    config('receiptprinter.connector_descriptor')
);

// Set store info
$printer->setStore($mid, $store_name, $store_address, $store_phone, $store_email, $store_website);

// Add items
foreach ($items as $item) {
    $printer->addItem(
        $item['name'],
        $item['qty'],
        $item['price']
    );
}
// Set tax
$printer->setTax($tax_percentage);

// Calculate total
$printer->calculateSubTotal();
$printer->calculateGrandTotal();

// Set transaction ID
$printer->setTransactionID($transaction_id);

// Set qr code
$printer->setQRcode([
    'tid' => $transaction_id,
]);

// Print receipt
$printer->printReceipt();
}
}
